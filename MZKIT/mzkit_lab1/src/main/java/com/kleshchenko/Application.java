package com.kleshchenko;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.Marker;
import org.apache.logging.log4j.message.EntryMessage;
import org.apache.logging.log4j.message.Message;
import org.apache.logging.log4j.message.MessageFactory;
import org.apache.logging.log4j.util.MessageSupplier;
import org.apache.logging.log4j.util.Supplier;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import javax.imageio.ImageIO;

public class Application {
    private static final Logger LOGGER = LogManager.getLogger(Application.class);
    public static void main(String[] args) throws IOException {
        BufferedImage image = ImageIO.read(new File("src\\main\\resources\\lab1.png"));
        convertBMPtoTXT(image);
        LOGGER.info("Converting done.");
    }

    private static void convertBMPtoTXT(BufferedImage image) throws FileNotFoundException {
        int width = image.getWidth();
        int height = image.getHeight();
        PrintWriter pw = new PrintWriter(new File("results.txt"));

        for (int row = 0; row < height; row++) {
            for (int col = 0; col < width; col++) {
                Color c = new Color(image.getRGB(col, row));
                pw.println("pixel [" + row + "][" + col + "]");
                int red = c.getRed();
                pw.printf("| red=%08d\n", Integer.parseInt(Integer.toBinaryString(red)));
                int green = c.getGreen();
                pw.printf("| green=%08d\n", Integer.parseInt(Integer.toBinaryString(green)));
                int blue = c.getBlue();
                pw.printf("| blue=%08d\n", Integer.parseInt(Integer.toBinaryString(blue)));
                pw.println("");
            }
        }
    }
}
